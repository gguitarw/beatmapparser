﻿#include "pch.h"
#include "beatmap.h"

using beatmap_parser::HitObject;
using beatmap_parser::TimingPoint;
using spud_base::point::Point;

TEST(TimingPointConstructor, InheritedFrom) {
    // for map Sidetracked Day
    const double slider_multiplier = 1.9;

    // first timing point in Sidetracked Day
    TimingPoint timing_point(R"(2146,638.297872340426,4,2,2,15,1,0)", slider_multiplier);
    // TimingPoint timing_point(R"(2146,638.297872340426,4,2,2,15,1,0)");
    EXPECT_EQ(timing_point.offset_from_start, 2146);
    EXPECT_EQ(timing_point.milliseconds_per_beat, 638.297872340426);
}

TEST(TimingPointConstructor, Inherited) {
    // for map Sidetracked Day
    const double slider_multiplier = 1.9;

    // second timing point in Sidetracked Day
    TimingPoint timing_point(R"(12358,-100,4,2,2,25,0,0)", slider_multiplier);
    // TimingPoint timing_point(R"(12358,-100,4,2,2,25,0,0)");
    EXPECT_EQ(timing_point.offset_from_start, 12358);
    EXPECT_EQ(timing_point.milliseconds_per_beat, -100);
}

TEST(TimingPointInheritence, ProcessedCorrectly) {
    // for map Sidetracked Day
    const double slider_multiplier = 1.9;

    std::vector<TimingPoint> timing_points;

    // emplace_back has an issue with second argument, use push_back instead
    timing_points.push_back({ R"(2146,638.297872340426,4,2,2,15,1,0)", slider_multiplier });   // 0
    timing_points.push_back({ R"(12358,-100,4,2,2,25,0,0)", slider_multiplier });              // 1
    timing_points.push_back({ R"(34699,-100,4,2,2,25,0,0)", slider_multiplier });              // 2
    timing_points.push_back({ R"(42998,319.148936170213,4,2,2,30,1,0)", slider_multiplier });  // 3
    timing_points.push_back({ R"(42998,-200,4,2,2,30,0,0)", slider_multiplier });              // 4
    timing_points.push_back({ R"(62306,-100,4,2,1,60,0,0)", slider_multiplier });              // 5
    timing_points.push_back({ R"(82572,-133.333333333333,4,2,1,30,0,0)", slider_multiplier }); // 6

    TimingPoint::ProcessInheritedPoints(timing_points);

    // .at(0), inherited from
    EXPECT_EQ(timing_points.at(0).offset_from_start, 0);
    EXPECT_EQ(timing_points.at(0).milliseconds_per_beat, 638.297872340426);
    EXPECT_EQ(timing_points.at(0).beats_per_measure, 4);
    EXPECT_EQ(timing_points.at(0).slider_velocity_multiplier, 1.9);

    // .at(1)
    EXPECT_EQ(timing_points.at(1).offset_from_start, 12358);
    EXPECT_EQ(timing_points.at(1).milliseconds_per_beat, 638.297872340426);
    EXPECT_EQ(timing_points.at(1).beats_per_measure, 4);
    EXPECT_EQ(timing_points.at(1).slider_velocity_multiplier, 1.9);

    // .at(2)
    EXPECT_EQ(timing_points.at(2).offset_from_start, 34699);
    EXPECT_EQ(timing_points.at(2).milliseconds_per_beat, 638.297872340426);
    EXPECT_EQ(timing_points.at(2).beats_per_measure, 4);
    EXPECT_EQ(timing_points.at(2).slider_velocity_multiplier, 1.9);

    // .at(3), inherited from
    EXPECT_EQ(timing_points.at(3).offset_from_start, 42998);
    EXPECT_EQ(timing_points.at(3).milliseconds_per_beat, 319.148936170213);
    EXPECT_EQ(timing_points.at(3).beats_per_measure, 4);
    EXPECT_EQ(timing_points.at(3).slider_velocity_multiplier, 1.9);

    // .at(4)
    EXPECT_EQ(timing_points.at(4).offset_from_start, 42998);
    EXPECT_EQ(timing_points.at(4).milliseconds_per_beat, 638.297872340426);
    EXPECT_EQ(timing_points.at(4).beats_per_measure, 4);
    EXPECT_EQ(timing_points.at(4).slider_velocity_multiplier, 1.9);

    // .at(5)
    EXPECT_EQ(timing_points.at(5).offset_from_start, 62306);
    EXPECT_EQ(timing_points.at(5).milliseconds_per_beat, 319.148936170213);
    EXPECT_EQ(timing_points.at(5).beats_per_measure, 4);
    EXPECT_EQ(timing_points.at(5).slider_velocity_multiplier, 1.9);

    // .at(6)
    EXPECT_EQ(timing_points.at(6).offset_from_start, 82572);
    EXPECT_NEAR(timing_points.at(6).milliseconds_per_beat, 425.5319148936173333333333, 0.01); // ~319 * 4/3
    // EXPECT_EQ(timing_points.at(6).milliseconds_per_beat, 425.531914893617); // ~319 * 4/3
    EXPECT_EQ(timing_points.at(6).beats_per_measure, 4);
    EXPECT_EQ(timing_points.at(6).slider_velocity_multiplier, 1.9);
}

TEST(HitObjectConstructor, Circle) {
    // some circle in Sidetracked Day, line 132 of the .osu file
    HitObject hit_object(R"(175,372,47785,1,2,0:0:0:0:)");
    EXPECT_TRUE(hit_object.coord == Point<int>(175, 372));
    EXPECT_EQ(hit_object.timing, 47785);
    EXPECT_TRUE(hit_object.type & beatmap_parser::kCircle);
}

TEST(HitObjectConstructor, Slider) {
    // first slider of Sidetracked Day
    HitObject slider_1(R"(64,296,17465,6,0,B|60:262|30:240|30:240|48:210|42:176|42:176|84:161|98:131|98:131|177:203|277:156|277:156|232:160|201:126,1,475,2|0,0:0|0:0,0:0:0:0:)");
    EXPECT_EQ(slider_1.timing, 17465);
    EXPECT_TRUE(slider_1.coord == Point<int>(64, 296));
    EXPECT_TRUE(slider_1.type & beatmap_parser::kSlider);
    EXPECT_EQ(slider_1.slider_data.slider_type, 'B');
    EXPECT_TRUE(slider_1.slider_data.slider_points.at(0) == Point<int>(60, 262));

    // second slider of Sidetracked Day
    HitObject slider_2(R"(275,101,20018,6,0,B|315:90|354:111|354:111|411:94|419:41|419:41|422:96|479:132,1,285,2|2,0:0|0:0,0:0:0:0:)");
    EXPECT_TRUE(slider_2.coord == Point<int>(275, 101));
    EXPECT_EQ(slider_2.timing, 20018);
    EXPECT_TRUE(slider_2.type & beatmap_parser::kSlider);
    // slider stuff
    EXPECT_EQ(slider_2.slider_data.slider_type, 'B');
    EXPECT_TRUE(slider_2.slider_data.slider_points.at(0) == Point<int>(315, 90));
    EXPECT_EQ(slider_2.slider_data.slider_repeats, 1);
    EXPECT_EQ(slider_2.slider_data.slider_length, 285);
}

TEST(HitObjectConstructor, Spinner) {
    // first spinner of Sidetracked Day
    HitObject hit_object(R"(256,192,12358,12,0,17146,0:0:0:0:)");
    EXPECT_TRUE(hit_object.coord == Point<int>(256, 192));
    EXPECT_EQ(hit_object.timing, 12358);
    EXPECT_TRUE(hit_object.type & beatmap_parser::kSpinner);
    EXPECT_EQ(hit_object.spinner_end_time, 17146);
}
