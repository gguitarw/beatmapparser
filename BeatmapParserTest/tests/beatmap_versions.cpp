#include "pch.h"
#include "beatmap.h"

using beatmap_parser::Beatmap;

bool AllSlidersHaveTimingPoints(const Beatmap& beatmap) {
    bool all_have_timing_points = true;
    for (const auto& hit_object : beatmap.hit_objects) {
        if (hit_object.type & beatmap_parser::kSlider) {
            if (hit_object.slider_data.timing_point == nullptr) {
                all_have_timing_points = false;
            }
        }
    }
    return all_have_timing_points;
}

TEST(BeatmapLoad, v5) {
    const std::string file_path = R"(beatmaps\v5\Peter Lambert - osu! tutorial (peppy) [Gameplay basics].osu)";
    Beatmap tutorial(file_path);

	EXPECT_EQ(tutorial.information.artist, "Peter Lambert");
	EXPECT_EQ(tutorial.information.creator, "peppy");
	EXPECT_EQ(tutorial.difficulty.slider_multiplier, 0.6);
	EXPECT_EQ(tutorial.difficulty.ar, 0); // ar = od when version < 8
	EXPECT_EQ(tutorial.hit_objects.at(2).type, beatmap_parser::kCircle);
	EXPECT_EQ(tutorial.hit_objects.at(2).timing, 33165);
    EXPECT_TRUE(AllSlidersHaveTimingPoints(tutorial));
}

// TODO: write tests for each of the file versions between these...
// take note of the differences in the "Version Differences.txt" file in the beatmaps folder

TEST(BeatmapLoad, v14_SideTracked) {
    const std::string file_path = R"(beatmaps\v14\VINXIS - Sidetracked Day (SnowNiNo_) [THREE DIMENSIONS].osu)";
	Beatmap sidetracked_day(file_path);
	EXPECT_EQ(sidetracked_day.hit_objects.at(10).timing, 25125);
	EXPECT_EQ(sidetracked_day.hit_objects.at(10).slider_data.timing_point->offset_from_start, 24486);
	EXPECT_EQ(sidetracked_day.hit_objects.at(10).slider_data.slider_points.at(4).y, 109);
    EXPECT_TRUE(AllSlidersHaveTimingPoints(sidetracked_day));
}

TEST(BeatmapLoad, v14_TemShop) {
    const std::string file_path = R"(C:\Users\Garrett\Documents\Programming\osu_spud\MSVC-dev\BeatmapTest\TestBeatmapFiles\v14\toby fox - Tem Shop (-Faded-) [Insane].osu)";
    Beatmap tem_shop(file_path);
    EXPECT_EQ(tem_shop.hit_objects.at(4).timing, 1701);
    EXPECT_EQ(tem_shop.hit_objects.at(4).slider_data.timing_point->offset_from_start, 0); // first timing point, offset should be 0
    EXPECT_EQ(tem_shop.hit_objects.at(4).slider_data.slider_points.at(2).y, 168);
    EXPECT_TRUE(AllSlidersHaveTimingPoints(tem_shop));
}