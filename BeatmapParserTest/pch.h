//
// pch.h
// Header for standard system include files.
//

#pragma once

// this is due to an internal gtest usage, not going to mess with it
#define _SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING

#include "gtest/gtest.h"
