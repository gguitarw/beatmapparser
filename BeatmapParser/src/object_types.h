﻿#pragma once

#include <vector>
#include <memory>
#include "point/point.h"
#include "strings/string_utils.h"

using spud_base::util::SplitString;
using spud_base::point::Point;

// TODO: rework namespaces to be more consistent (for all spud projects)
//      spud_base::point::Point -> spud::base::Point;
//      beatmap_parser::Beatmap -> spud::beatmap_parser::Beatmap;

// reference the osu! wiki for specific details on timing points and hit objects
// https://github.com/ppy/osu-wiki/tree/master/wiki/osu!_File_Formats/

namespace beatmap_parser {
    enum HitObjectType {
        kCircle = 1,
        kSlider = 2,
        kNewCombo = 4,
        kSpinner = 8,
        kManiaHold = 128
    };

    class TimingPoint {
    public:
        int64_t offset_from_start; // time in milliseconds since song started
        int beats_per_measure;
        double milliseconds_per_beat;
        double slider_velocity_multiplier; // used in slider duration calculation, stored here so we don't need beatmap to calculate

        // constructor for creating TimingPoints from strings (lines from beatmap file) and beatmap's velocity multiplier
        TimingPoint(const std::string& line, const double velocity_multiplier) : TimingPoint(SplitString(line, ','), velocity_multiplier) {}
        TimingPoint(const std::vector<std::string>& data, const double velocity_multiplier);

        // used in sorts/searches, compare timing values between two hit objects
        bool operator<(const TimingPoint& compare) const {
            return this->offset_from_start < compare.offset_from_start;
        }
        
        // process timing point inheritence, modifies timing_point's ms_per_beat
        static void ProcessInheritedPoints(std::vector<TimingPoint>& timing_points);
    };

    struct SliderObject {
        // stores data relevant only to sliders
        // TODO: rename these to remove redundant slider_ prefix
        char slider_type; // B = bezier, P = perfect (circular), C = catmull
        std::vector<Point<int>> slider_points; // TODO: refactor/rename to control_points
        int slider_repeats; // how many times the sliderball moves across slider path (1 = no repeats)
        double slider_length; // length in osu! pixels
        std::shared_ptr<TimingPoint> timing_point;

        double Duration() const {
            return slider_length /
                   (100.0 * timing_point->slider_velocity_multiplier) *
                   timing_point->milliseconds_per_beat;
        }
    };

    class HitObject {
    public:
        Point<int> coord; // coordinates on osu playfield, x: [0, 512], y: [0, 384]
        int64_t timing; // when the object should be clicked, in ms since the start of the map
        HitObjectType type; // bitmask of circle, slider, newcombo, spinner, and mania hold notes

        // slider only
        SliderObject slider_data;

        // spinner only
        int64_t spinner_end_time;

        // constructor for creating HitObjects from strings (lines from beatmap file)
        HitObject(const std::string& line) : HitObject(SplitString(line, ',')) {}
        HitObject(const std::vector<std::string>& data);

        // used in sorts/searches, compare timing values between two hit objects
        bool operator<(const HitObject& hit_object) const {
            return this->timing < hit_object.timing;
        }

        // assigns timing points to sliders in hit_objects
        static void AssignTimingPoints(std::vector<HitObject>& hit_objects, const std::vector<TimingPoint>& timing_points);
    };
}
