﻿#pragma once

#include <fstream>
#include <vector>
#include "song.h"
#include "object_types.h"
#include <unordered_map>

namespace beatmap_parser {

	class Beatmap {
	public:
		// metadata
		SongInformation information;
		SongDifficulty difficulty;

		std::vector<TimingPoint> timing_points;
		std::vector<HitObject> hit_objects;

        // constructing the Beatmap does everything to load and parse it
		Beatmap(const std::string& file_path);

        // copy and move operations
	    Beatmap(const Beatmap& other)
	        : information(other.information),
	          difficulty(other.difficulty),
	          timing_points(other.timing_points),
	          hit_objects(other.hit_objects) {}

	    Beatmap(Beatmap&& other) noexcept
	        : information(std::move(other.information)),
	          difficulty(std::move(other.difficulty)),
	          timing_points(std::move(other.timing_points)),
	          hit_objects(std::move(other.hit_objects)) {}

	    Beatmap& operator=(const Beatmap& other) {
	        if (this == &other)
	            return *this;
	        information = other.information;
	        difficulty = other.difficulty;
	        timing_points = other.timing_points;
	        hit_objects = other.hit_objects;
	        return *this;
	    }

	    Beatmap& operator=(Beatmap&& other) noexcept {
	        if (this == &other)
	            return *this;
	        information = std::move(other.information);
	        difficulty = std::move(other.difficulty);
	        timing_points = std::move(other.timing_points);
	        hit_objects = std::move(other.hit_objects);
	        return *this;
	    }

	private:
		std::ifstream input_file_;

        // used for calculating inherited timing point's values
		std::unique_ptr<TimingPoint> previous_inherited_timing_point_;

        bool ParseBeatmap();

		void AddHitObject(const std::vector<std::string>& data);
		void AddTimingPoint(const std::vector<std::string>& data);
		void StoreParsedFields(const std::unordered_map<std::string, std::string>& map);

	    TimingPoint GetCurrentTimingPoint(const int64_t time) const; 
	};
}
