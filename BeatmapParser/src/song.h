﻿#pragma once
#include <string>

namespace beatmap_parser {
    enum GameMode { // TODO: move this to spud_base
        kStandard = 0,
        kTaiko = 1,
        kCtb = 2,
        kMania = 3
    };

    struct TimingWindow {
        const double k300;
        const double k100;
        const double k50;

        TimingWindow(const double i300, const double i100, const double i50) :
            k300(i300), k100(i100), k50(i50) { }
    };

    struct SongInformation {
        std::string title;
        std::string artist;
        std::string creator;
        std::string difficulty;
        GameMode mode;
        int version = -1; // version of map file format
        double stack_leniency;
        std::string id;
        std::string set_id;
        std::string file_path;

        explicit SongInformation(const std::string& file_path)
            : file_path(file_path) {}
    };

    class SongDifficulty {
    public:
        double hp;                // health drain
        double cs;                // circle size (large num is smaller circle)
        double od;                // timing leniency
        double ar;                // approach rate (higher is faster)
        double slider_multiplier; // speed of slider, how many osu!pixels/beat
        double slider_tick_rate;   // how many ticks per beat of a slider, not including ends

        // get the radius in osu!pixels
        inline double CsToPixels() const {
            return 54.4 - 4.48 * cs;
        }

        // the time when the circle starts fading in
        inline double ArToApproachTime() const {
            if (ar < 5) {
                return 1200 + 600 * (5 - ar) / 5;
            } else {
                return 1200 - 750 * (ar - 5) / 5;
            }
        }

        // the time when the circle is fully faded in
        inline double ArToFullFadeTime() const {
            if (ar < 5) {
                return 800 + 400 * (5 - ar) / 5;
            } else {
                return 800 - 500 * (ar - 5) / 5;
            }
        }

        // the width of the hit window
        inline TimingWindow OdToTimingWindow() const {
            return {
                50 + 30 * (5 - od) / 5,
                100 + 40 * (5 - od) / 5,
                150 + 50 * (5 - od) / 5
            };
        }

        // TODO: methods to change applied mods (and variable to keep track of what's been applied)
    };
}
