﻿#include "beatmap.h"
#include <sstream>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include "strings/string_utils.h"

using beatmap_parser::Beatmap;
using spud_base::util::SplitString;

Beatmap::Beatmap(const std::string& file_path) : information(file_path), difficulty() {
    ParseBeatmap();
}

bool Beatmap::ParseBeatmap() {
    input_file_.open(information.file_path, std::ios::in);

    if (!input_file_.is_open()) {
        std::cout << "Couldn't load beatmap from\n\t<" << information.file_path << ">" << std::endl;
        return false;
    }

    std::string line;
    std::string current_section;

    // parse the first line of the file to get the .osu format version
    std::getline(input_file_, line);
    if (line.find("osu file format") == std::string::npos) {
        std::cout << "Couldn't find beatmap file version" << std::endl;
        return false;
    }
    information.version = std::stoi(line.substr(line.find(" v") + 2));

    // unordered map for easy retrieval later
    // not just for the "Metadata" section, this stores any key/value pairs in the .osu file
    //      examples: 
    //          PreviewTime: 88317
    //          Artist:VINXIS
    //          etc...
    std::unordered_map<std::string, std::string> metadata_map;

    while (input_file_.good()) {
        std::getline(input_file_, line);

        if (line.empty()) {
            continue;
        } else if (line.front() == '[' && line.back() == ']') {
            // section heading
            current_section = line.substr(1, line.length() - 2);
            if (current_section == "TimingPoints") {
                // past the sections with metadata, so go through the unordered_map and store important things
                StoreParsedFields(metadata_map);
            }
            continue;
        }

        // do different work depending on the current section
        if (current_section == "HitObjects") {
            hit_objects.emplace_back(line);
        } else if (current_section == "TimingPoints") {
            timing_points.emplace_back(line, difficulty.slider_multiplier);
        } else if (current_section == "General"
                   || current_section == "Metadata"
                   || current_section == "Difficulty") {
            metadata_map.emplace(line.substr(0, line.find_first_of(':')),
                                  line.substr(line.find_first_of(':') + 1));
        }
    }

    // finishing touches to timing points and hit objects
    TimingPoint::ProcessInheritedPoints(timing_points);
    HitObject::AssignTimingPoints(hit_objects, timing_points);

    // done parsing
    std::cout << "Parsed beatmap: " << information.title << " - " << information.artist << '\n'
	    << hit_objects.size() << " hit objects, " << timing_points.size() << " timing points" << std::endl;
    input_file_.close();

    return true;
}

void Beatmap::StoreParsedFields(const std::unordered_map<std::string, std::string>& map) {
    // information
    information.title = map.at("Title");
    information.artist = map.at("Artist");
    information.creator = map.at("Creator");
    information.difficulty = map.at("Version");

    if (information.version >= 5) {
        information.mode = static_cast<GameMode>(std::stoi(map.at("Mode")));
        information.stack_leniency = std::stod(map.at("StackLeniency"));
    } else {
        information.mode = kStandard;
        information.stack_leniency = 1;
    }

    if (information.version >= 10) {
        information.id = map.at("BeatmapID");
        information.set_id = map.at("BeatmapSetID");
    }

    // difficulty
    difficulty.hp = std::stod(map.at("HPDrainRate"));
    difficulty.cs = std::stod(map.at("CircleSize"));
    difficulty.od = std::stod(map.at("OverallDifficulty"));

    if (information.version >= 8) {
        difficulty.ar = std::stod(map.at("ApproachRate"));
    } else {
        difficulty.ar = difficulty.od;
    }

    difficulty.slider_multiplier = std::stod(map.at("SliderMultiplier"));
    difficulty.slider_tick_rate = std::stod(map.at("SliderTickRate"));
}
