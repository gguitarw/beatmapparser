﻿#include "object_types.h"
#include "strings/string_utils.h"

using spud_base::util::SplitString;

beatmap_parser::TimingPoint::TimingPoint(const std::vector<std::string>& data, const double velocity_multiplier) {
    // TODO: checks for invalid data input

    offset_from_start = std::stoi(data.at(0));
    beats_per_measure = std::stoi(data.at(2));
    milliseconds_per_beat = std::stod(data.at(1));
    slider_velocity_multiplier = velocity_multiplier;
}

void beatmap_parser::TimingPoint::ProcessInheritedPoints(std::vector<TimingPoint>& timing_points) {
    // TODO: write tests for first timing point check, inherited point check, uninherited point

    // also sets first timing_point's offset to 0, per the spec from osu! website
    timing_points.at(0).offset_from_start = 0;

    size_t inherit_from_index = 0;
    for (size_t i = 0; i < timing_points.size(); ++i) {
        if (timing_points.at(i).milliseconds_per_beat < 0) {
            // can't be inherited from by other timing points
            const double ms_per_beat_scale = -timing_points.at(i).milliseconds_per_beat / 100;
            timing_points.at(i).milliseconds_per_beat = timing_points.at(inherit_from_index).milliseconds_per_beat * ms_per_beat_scale;
        } else {
            // positive ms_per_beat, other points inherit from this one
            inherit_from_index = i;
        }
    }
}

beatmap_parser::HitObject::HitObject(const std::vector<std::string>& data) {
    // TODO: checks for invalid data input
    coord = { std::stoi(data.at(0)), std::stoi(data.at(1)) };
    timing = std::stoi(data.at(2));
    type = static_cast<HitObjectType>(std::stoi(data.at(3)));

    // .type is a bitmask
    if (type & kCircle) {
        // nothing special about a regular circle
    } else if (type & kSlider) {
        // sliders get special treatment, they're a bit more complicated
        slider_data.slider_type = data.at(5)[0];
        slider_data.slider_repeats = std::stoi(data.at(6));
        slider_data.slider_length = std::stod(data.at(7));
        slider_data.timing_point = nullptr; // this gets assigned outside of constructor, don't have enough info to do it here

        // parse slider points
        std::string slider_points = data.at(5);
        slider_points.erase(0, 2);
        std::vector<std::string> split_points = SplitString(slider_points, '|');
        for (const auto& point : split_points) {
            std::vector<std::string> coords = SplitString(point, ':');
            slider_data.slider_points.emplace_back(
                std::stoi(coords.at(0)),
                std::stoi(coords.at(1))
            );
        }
    } else if (type & kSpinner) {
        spinner_end_time = std::stoi(data.at(5));
    }
}

void beatmap_parser::HitObject::AssignTimingPoints(std::vector<HitObject>& hit_objects, const std::vector<TimingPoint>& timing_points) {
    size_t timing_point_index = 0;
    for (auto& hit_object : hit_objects) {
        if (!(hit_object.type & kSlider)) {
            // not a slider, don't need to add timing point
            // TODO: this chould eventually be replaced by calling GetNextSlider() or something instead of for each loop
            continue;
        }

        while (timing_point_index < timing_points.size() - 1 &&
            hit_object.timing >= timing_points.at(timing_point_index + 1).offset_from_start) {
            // use the next timing point instead of the current one
            ++timing_point_index;
        }

        hit_object.slider_data.timing_point = std::make_shared<TimingPoint>(timing_points.at(timing_point_index));
    }
}
